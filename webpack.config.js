const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    main: [
      'whatwg-fetch',
      '@babel/polyfill',
      './components/App.js'
    ]
  },
  output: {
    path: path.resolve(__dirname, 'public/js'),
    filename: '[name].js'
  },
  module: {
   // configuration regarding modules
   rules: [
      // rules for modules (configure loaders, parser options, etc.)
      {
        // flags to apply these rules, even if they are overridden (advanced option)
        loader: 'babel-loader',
        // the loader which should be applied, it'll be resolved relative to the context
        // -loader suffix is no longer optional in webpack2 for clarity reasons
        // see webpack 1 upgrade guide
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          plugins: [
            ["@babel/plugin-proposal-decorators", { "legacy": true }],
            ["@babel/plugin-proposal-class-properties", { "loose": true }],
            ["import", {
              "libraryName": "antd",
              "libraryDirectory": "es",
              "style": "css"
            }]
          ]
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
   ]
  }
}
