import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Card, Button, message } from 'antd'
const { Meta } = Card

@connect((store) => {
  return {
    user: store.user,
    items: store.items,
  };
})

export default class Item extends Component {

  onCart = (e) => {
    const { item } = this.props;
    fetch(`/cart`, {
      method: 'put', 
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }, body: JSON.stringify({
        itemId: item.id,
      }),
    }).then(res => res.json())
    .then(({success, result}) => {
      if (!success) {
        message.error(result.message);
        return;
      } message.success(`${
        item.title
      } has been added!`);
      this.props.dispatch({
        type: 'UPDATE_CART',
        payload: [],
      });
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  render() {
    const { item } = this.props;
    return (
      <Card
        hoverable
        className='cat-item'
        cover={<img alt={item.title} src={item.image} />}
      >
        <Meta title={
          `${item.title} - $${Number(item.price).toFixed(2)}`
        } description={
          <ul className='item-info'>
            <li>Available: {item.quant}</li>
            <li>Location: {item.zip}</li>
            <li>
              <Button type='primary' onClick={this.onCart}>Add to Cart</Button>
            </li>
          </ul>
        }/>
      </Card>
    );
  }
}