import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Empty, message } from 'antd'

import Item from './Item'
import NewItem from './NewItem'

import { IDB } from '../../utils/idb'

@connect((store) => {
  return {
    user: store.user,
    items: store.items,
    current: store.current,
  };
})

export default class Catalog extends Component {

  componentDidMount () {
    let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
    if (idb) {
      idb = new IDB(idb);
      idb.upgradeDB(idb.version, 1);
      idb.getAll('items', (data, db) => {
        this.props.dispatch({
          type: 'UPDATE_ITEMS',
          payload: data,
        });
      });
    } else this.preFetch();
  }

  preFetch = () => {
    let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
    if (idb) {
      idb = new IDB(idb);
      idb.upgradeDB(idb.version, 1);
    } fetch(`/items${user && user.zip ? `?zip=${user.zip}`: ''}`)
    .then(res => res.json()).then(({success, result}) => {
      if (!success) {
        message.error(result.message);
        return;
      }

      this.props.dispatch({
        type: 'UPDATE_ITEMS',
        payload: result,
      });

      if (idb) {
        result.forEach(item => {
          idb.putObject('items', item);
        });
      }
    }).catch(() => {
      if (idb) {
        idb.getAll('items', (data, db) => {
          this.props.dispatch({
            type: 'UPDATE_ITEMS',
            payload: data,
          });
        });
      } else message.error('Network error...try again later!');
    });
  }

  update = (url) => {
    let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
    if (idb) {
      idb = new IDB(idb);
      idb.upgradeDB(idb.version, 1);
    } fetch(url)
    .then(res => res.json())
    .then(({success, result}) => {
      if (!success) {
        message.error(result.message);
        return;
      }

      this.props.dispatch({
        type: 'UPDATE_ITEMS',
        payload: result,
      });

      if (idb) {
        result.forEach(item => {
          idb.putObject('items', item);
        });
      }
    }).catch(() => {
      if (idb) {
        idb.getAll('items', (data, db) => {
          this.props.dispatch({
            type: 'UPDATE_ITEMS',
            payload: data,
          });
        });
      } else message.error('Network error...try again later!');
    });
  }

  render () {
    const { user, items } = this.props;
    const children = [];

    if (user.id && user.login) children.push(
      <NewItem key='new-item' update={this.update}/>
    );
    for (let i of items) {
      children.push(<Item key={`item-${i.id}`} item={i}/>);
    } return <div className='no-data'>{
      children.length ? children : (<Empty />)
    }</div>;
  }
}
