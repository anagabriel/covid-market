import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Card, Select, Input, Button, message } from 'antd'

import ItemImage from './ItemImage'

const { Meta } = Card
const { Option } = Select
const children = [
  <Option key='cat-1'>TP &amp; Wipes</Option>,
  <Option key='cat-2'>Paper Towels</Option>,
  <Option key='cat-3'>Tissues</Option>,
  <Option key='cat-4'>Gloves</Option>,
  <Option key='cat-5'>Masks</Option>,
];

@connect((store) => {
  return {
    user: store.user,
    current: store.current,
  };
})

export default class NewItem extends Component {
  state = {
    category: [],
    image: null,
  };

  onChange = (value, key) => {
    if (key == 'image')
      this.setState({ image: value });
    else
      this.setState({ category: value });
  }

  onSave = (e) => {
    const { category } = this.state;
    const { user, update, current } = this.props;

    if (!user.zip) {
      message.error('Please, set the zip code in personal information!');
      return;
    }

    const url = document.getElementById('url');
    const title = document.getElementById('title');
    const quant = document.getElementById('quant');
    const price = document.getElementById('price');

    let errors = 0;
    if (url.value == '' || !url.value) {
      errors++;
      url.classList.add('required');
      message.error('Add an image or URL!');
    } else if (url.classList.contains('required')) {
      url.classList.remove('required');
    } if (title.value == '' || !title.value) {
      errors++;
      title.classList.add('required');
      message.error('Add a title!');
    } else if (title.classList.contains('required')) {
      title.classList.remove('required');
    } if (!quant.value) {
      errors++;
      quant.classList.add('required');
      message.error('Add the quantity you have available!');
    } else if (quant.classList.contains('required')) {
      quant.classList.remove('required');
    } if (!price.value && price.value !== 0) {
      errors++;
      price.classList.add('required');
      message.error('Add a valid price for your product!');
    } else if (price.classList.contains('required')) {
      price.classList.remove('required');
    } if (!category.length) {
      errors++;
      message.error('Select a category for your product!');
    }
    
    if (errors) return;
    fetch(`/items`, { 
      method: 'put',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }, body: JSON.stringify({
        zip: user.zip,
        owner: user.id,
        image: url.value,
        title: title.value,
        quant: quant.value,
        price: price.value,
        category: category.join(';'),
      }),
    }).then(res => res.json())
    .then(({success, result}) => {
      if (success) {
        update(`/items?zip=${user.zip}${
          current && current.length ? `&category=${current.map(c => `cat-${c}`).join(';')}` : ''
        }`);
        message.success(`${
          title.value
        } has been added to ${
          this.props.user.zip
        } catalog!`);
        this.setState({
          category: [],
          image: null,
        });
      } else message.error(result.message);
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  render () {
    const { image } = this.state;
    return (
      <Card hoverable className='cat-item new-item' cover={<ItemImage change={this.onChange}/>}>
        <Meta title={
          image ? <Input id='url' type='url' value={image}/> :
                  <Input id='url' type='url' placeholder='Or, URL to an image'/>
        } description={
          <div className='item-info'>
            <Input id='title' type='text' placeholder='Title'/>
            <Input id='quant' type='number' placeholder='Quantity'/>
            <Input id='price' type='number' placeholder='Price'/>
            <Select
              mode="multiple"
              placeholder="Select a category"
              defaultValue={[]}
              onChange={this.onChange}
            >{children}</Select>
            <Button type='primary' onClick={this.onSave}>Save</Button>
          </div>
        }/>
      </Card>
    );
  }
}