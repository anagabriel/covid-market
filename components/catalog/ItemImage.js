import React, { Component } from 'react'

import { Upload, Icon, message } from 'antd'

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

export default class ItemImage extends Component {
  
  state = { loading: false };

  beforeUpload = (file) => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
  }

  handleChange = info => {
    if (info.file.status === 'uploading') {
      this.setState({ loading: true });
      return;
    } if (info.file.status === 'done') {
      getBase64(info.file.originFileObj, imageUrl => {
        const url = info.file.response.result.url;
        this.props.change(url, 'image');
        this.setState({
          imageUrl,
          loading: false,
        });
      });
    }
  };

  render() {
    const { imageUrl } = this.state;
    const uploadButton = (
      <div>
        {this.state.loading ? <Icon type='lock' /> : <Icon type='plus' />}
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    
    return (
      <Upload
        name="avatar"
        action="/items/image"
        showUploadList={false}
        listType="picture-card"
        className="avatar-uploader"
        beforeUpload={this.beforeUpload}
        onChange={this.handleChange}
      >{
        imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton
      }</Upload>
    );
  }
}