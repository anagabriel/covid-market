import React, { Component } from 'react'
import { connect } from 'react-redux'

import { 
  Form, 
  Icon, 
  Input, 
  Button, 
  message, 
} from 'antd'

import { isEmailValid } from '../../utils/std'

@connect((store) => {
  return {
    user: store.user,
    page: store.page,
  };
})

export default class Login extends Component {

  onPassword = (e) => {
    const code = document.getElementById('code');
    const email = document.getElementById('email');
    const confirm = document.getElementById('confirm');
    const password = document.getElementById('password');

    let errors = false;
    if (email.value == '' || !email.value || !isEmailValid(email.value)) {
      errors++;
      email.classList.add('required');
      message.error('Your email is missing!');
    } else if (email.classList.contains('required')) {
      email.classList.remove('required');
    } if (password.value == '' || !password.value) {
      errors++;
      password.classList.add('required');
      message.error('Your password is missing!');
    } else if (password.classList.contains('required')) {
      password.classList.remove('required');
    }  if (confirm.value == '' || !confirm.value || confirm.value !== confirm.value) {
      errors++;
      confirm.classList.add('required');
      message.error('Please, re-enter your password!');
    } else if (confirm.classList.contains('required')) {
      confirm.classList.remove('required');
    } if (code.value == '' || !code.value) {
      errors++;
      code.classList.add('required');
      message.error('Please, enter your authentication code!');
    } else if (code.classList.contains('required')) {
      code.classList.remove('required');
    } 

    if (errors) return;
    fetch(`/reset`, { 
      method: 'post',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }, body: JSON.stringify({
        code: code.value,
        email: email.value,
        password: password.value,
        confirm: confirm.value,
      }), 
    }).then(res => res.json())
    .then(({success, result}) => {
      if (success) {
        message.success('Your password has been reset!');
        this.props.dispatch({
          type: 'LOGIN',
          payload: result,
        });

        this.props.dispatch({
          type: 'GOTO_PROFILE',
          payload: true,
        });
      } else message.error(result.message);
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  onReset = (e) => {
    const reset = confirm('Are you sure you want to reset your password?');
    const email = document.getElementById('email');
    const password = document.getElementById('password');
    const confirmp = document.getElementById('confirm');

    let errors = false;
    if (!reset) return;
    else if (email.value == '' || !email.value || !isEmailValid(email.value)) {
      errors = true;
      email.classList.add('required');
      message.error('Your email is missing!');
    } else if (email.classList.contains('required')) {
      email.classList.remove('required');
    } if (confirmp.classList.contains('required')) {
      confirmp.classList.remove('required');
    } if (password.classList.contains('required')) {
      password.classList.remove('required');
    } 

    if (errors) return;
    fetch(`/reset/${email.value}`)
    .then(res => res.json())
    .then(({success, result}) => {
      if (success) message.success('Check your email!');
      else message.error(result.message);
      this.props.dispatch({
        type: 'GOTO_RESET',
        payload: true,
      });
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  onSignUp = (e) => {
    const email = document.getElementById('email');
    const password = document.getElementById('password');
    const confirm = document.getElementById('confirm');

    let errors = 0;
    if (email.value == '' || !email.value || !isEmailValid(email.value)) {
      errors++;
      email.classList.add('required');
      message.error('Your email is missing!');
    } else if (email.classList.contains('required')) {
      email.classList.remove('required');
    } if (password.value == '' || !password.value) {
      errors++;
      password.classList.add('required');
      message.error('Your password is missing!');
    } else if (password.classList.contains('required')) {
      password.classList.remove('required');
    }  if (confirm.value == '' || !confirm.value || confirm.value !== confirm.value) {
      errors++;
      confirm.classList.add('required');
      message.error('Please, re-enter your password!');
    } else if (confirm.classList.contains('required')) {
      confirm.classList.remove('required');
    } 

    if (errors) return;
    fetch(`/users`, { 
      method: 'put',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }, body: JSON.stringify({
        email: email.value,
        password: password.value,
        confirm: confirm.value,
      }), 
    }).then(res => res.json())
    .then((res) => {
      if (res.success) {
        message.success(`${res.result.fullname} has successfully logged in!`);
        this.props.dispatch({
          type: 'LOGIN',
          payload: res.result,
        });

        this.props.dispatch({
          type: 'GOTO_PROFILE',
          payload: true,
        });
      } else message.error(res.result.message);
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  onSignIn = (e) => {
    const email = document.getElementById('email');
    const password = document.getElementById('password');
    const confirm = document.getElementById('confirm');

    let errors = 0;
    if (email.value == '' || !email.value || !isEmailValid(email.value)) {
      errors++;
      email.classList.add('required');
      message.error('Your email is missing!');
    } else if (email.classList.contains('required')) {
      email.classList.remove('required');
    } if (password.value == '' || !password.value) {
      errors++;
      password.classList.add('required');
      message.error('Your password is missing!');
    } else if (password.classList.contains('required')) {
      password.classList.remove('required');
    } if (confirm.classList.contains('required')) {
      confirm.classList.remove('required');
    } 
    
    if (errors) return;  
    fetch(`/users/login`, { 
      method: 'post',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }, body: JSON.stringify({
        email: email.value,
        password: password.value,
      }),
    }).then(res => res.json())
    .then((res) => {
      if (res.success) {
        message.success(`${res.result.fullname} has successfully logged in!`);
        this.props.dispatch({
          type: 'LOGIN',
          payload: res.result,
        });

        this.props.dispatch({
          type: 'GOTO_PROFILE',
          payload: true,
        });
      } else message.error(res.result.message);
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  render () {
    const { page } = this.props;
    if (page === 'reset')
      return (
        <Form className="login-form">
          <Form.Item>
            <Input 
              id='email'
              type='email'
              placeholder="Email Address" 
              prefix={<Icon type='user'/>} 
            />
          </Form.Item>
          <Form.Item>
            <Input
              id='code'
              type="text"
              placeholder="Authentication code"
              prefix={<Icon type='key'/>}
            />
          </Form.Item>
          <Form.Item>
            <Input
              id='password'
              type="password"
              placeholder="Password"
              prefix={<Icon type='lock'/>}
            />
          </Form.Item>
          <Form.Item>
            <Input
              id='confirm'
              type="password"
              placeholder="Re-enter Password"
              prefix={<Icon type='lock'/>}
            />
          </Form.Item>
          <div className='reset-password'>
            <Button type="primary" htmlType="submit" className="login-form-button" onClick={this.onPassword}>
              Reset Password
            </Button>
          </div>
        </Form>
      );
    else return (
      <Form className="login-form">
        <Form.Item>
          <Input 
            id='email'
            type='email'
            placeholder="Email Address" 
            prefix={<Icon type='user'/>} 
          />
        </Form.Item>
        <Form.Item>
          <Input
            id='password'
            type="password"
            placeholder="Password"
            prefix={<Icon type='lock'/>}
          />
        </Form.Item>
        <Form.Item>
          <Input
            id='confirm'
            type="password"
            placeholder="Re-enter Password"
            prefix={<Icon type='lock'/>}
          />
        </Form.Item>
        <div className='login-controls'>
          <Button type="primary" htmlType="submit" className="login-form-button" onClick={this.onSignIn}>
            Sign in
          </Button>
          <Button type="primary" htmlType="submit" className="login-form-button" onClick={this.onSignUp}>
            Sign up
          </Button>
        </div>
        <div className='reset-password'>
          <a className="login-form-forgot" onClick={this.onReset}>
            Reset password
          </a>
        </div>
      </Form>
    );
  }
}
