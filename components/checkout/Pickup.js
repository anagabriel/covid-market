import React, { Component } from 'react'
import inputs from '../utils/inputs'

export default class Pickup extends Component {

  componentDidMount() { }

  render() {
    return (
      <div className='form none'>
        <input id='date' name='date' type='date' />
        <input id='time' name='time' type='time' />
        <input id='location' name='location' placeholder='location' type='text' disabled/>
      </div>
    );
  }
}
