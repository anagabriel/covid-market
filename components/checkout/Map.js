import React, { Component } from 'react'
import inputs from '../utils/inputs'
import idb from 'idb'

export default class Map extends Component {
  constructor(props) {
    super(props);

    this.state = {
      map: null,
      service: null,
      center: { lat: 40.0309648, lng: -86.2979091 },
      list: [],
      selected: null
    };
  }

  componentDidMount() {
    inputs.addLoader();
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.updatePosition.bind(this));
    } else {
      document.getElementById('map').innerHTML = 'Geolocation is not supported by this browser.';
    } try {
      let map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: this.state.center
      });
      let service = new google.maps.places.PlacesService(map);
      this.setState({ map: map, service: service });
    } catch(e) { console.error(e); }

    fetch('/locations')
    .then(res => res.json())
    .then((res) => {
      if (!res.success) return console.log(res.result);

      const dbPromise = idb.open('oo-db-v1', 1, upgradeDB => {
        upgradeDB.createObjectStore('locations', { keyPath: 'id' });
      });

      dbPromise.then(db => {
        const tx = db.transaction('locations', 'readwrite');
        res.result.forEach(s => tx.objectStore('locations').put(s));
        return tx.complete;
      });

      return this.setState({ list: res.result });
    }).catch(() => {
      console.log('Network error..');

      const dbPromise = idb.open('oo-db-v1', 1, inputs.upgrade);

      dbPromise.then(db => {
        return db.transaction('locations')
                 .objectStore('locations')
                 .getAll();
      }).then(data => {
         this.setState({ list: data });
      });
    }).then(() => {
      inputs.removeLoader();
    });
  }

  locationClicked(marker) {
    if (this.props.add)
      this.props.add(marker.getPlace().placeId);
    else this.setState({
      selected: marker.getPlace().placeId
    });
  }

  updatePosition(position) {
    try {
      this.state.map.setCenter({
        lat: position.coords.latitude,
        lng: position.coords.longitude
      });
    } catch(e) {
      console.log(e);
    } finally {
      this.setState({
        lat: position.coords.latitude,
        lng: position.coords.longitude
      });
    }
  }

  addEvent() {
    let loc = this.state.list;

    for (let i of loc) {
      let l = { placeId : i.id };
      try {
        this.state.service.getDetails(l, (place, status) => {
          let location = new google.maps.Marker({
            map: this.state.map,
            title: place.name + '\n' + place.formatted_phone_number  + '\n' + place.formatted_address,
            place: {
              placeId: place.place_id,
              location: place.geometry.location
            }
          });

          location.addListener('click',
            this.locationClicked.bind(this, location)
          );
        });
      } catch(e) { console.log(e); }
    }
  }

  render() {
    this.addEvent();

    // TODO: add tiles with locations as new Component for admin

    return (
      <div className='content'>
        <div id='info'></div>
        <div id='map' alt='Google Maps of local restaurants' role='application'>
          Google Maps is not supported!
        </div>
      </div>
    )
  }
}
