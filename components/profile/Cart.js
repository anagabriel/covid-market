import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Empty, List } from 'antd'
import CartItem from './CartItem'

import { IDB } from '../../utils/idb'

@connect((store) => {
  return {
    user: store.user,
    cart: store.cart,
  };
})

export default class Cart extends Component {

  componentDidMount () {
    let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
    if (idb) {
      idb = new IDB(idb);
      idb.upgradeDB(idb.version, 1);
    } fetch(`/cart`)
    .then(res => res.json())
    .then(({success, result}) => {
      if (!success) return;
      this.props.dispatch({
        type: 'UPDATE_CART',
        payload: result,
      });
    }).catch(() => {
      if (idb) {
        idb.getAll('cart', (data, db) => {
          this.props.dispatch({
            type: 'UPDATE_CART',
            payload: data,
          });
        });
      } else message.error('Network error...try again later!');
    });
  }
  
  render() {
    const { cart } = this.props;
    return cart && cart.length ? <List
      itemLayout="horizontal"
      dataSource={cart}
      renderItem={item => <CartItem key={`cart-${item.id}`} item={item}/>}
    /> : <Empty/>;
  }
}
