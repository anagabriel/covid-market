import React, { Component } from 'react'
import { connect } from 'react-redux'

import moment from 'moment'
import { Card, Input, Button, Skeleton, TimePicker, DatePicker, message } from 'antd'

import { IDB } from '../../utils/idb'
const format = 'HH:mm'

@connect((store) => {
    return {
      user: store.user,
    };
})

export default class Request extends Component {

    state = { 
      date: new Date().toISOString(),
      item: null,
      time: '10:00',
      location: '',
    };

    componentDidMount () {
      const { order: { itemId } } = this.props;
      let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
      if (idb) {
        idb = new IDB(idb);
        idb.upgradeDB(idb.version, 1);
      } fetch(`/items/${itemId}`)
      .then(res => res.json())
      .then(({success, result}) => {
        if (!success) return;
        this.setState({ 
          item: result,
          ...this.props.order, 
        });
      }).catch(() => {
        if (idb) {
          idb.getObject(itemId, 'item', (data, db) => {
            this.setState({  
              item: data,
              ...this.props.order,
            });
          });
        } else message.error('Network error...try again later!');
      });
    }

    onDone = (e) => {
      const { order } = this.props;
      const { item } = this.state;
      let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
      if (idb) {
        idb = new IDB(idb);
        idb.upgradeDB(idb.version, 1);
      } fetch(`/done/${order.id}`, { 
        method: 'post',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        },
      }).then(res => res.json())
      .then(({success, result}) => {
        if (success) {
          message.success(`${item.title} has been delivered!`);
          this.dispatch({
            type: 'UPDATE_ORDERS',
            payload: [],
          }).then(refresh);
        } else message.error(result.message);
      }).catch(() => {
        message.error('Network error...try again later!');
      });
    }

    onAccept = (e) => {
      const { item } = this.state;
      const { order, refresh } = this.props;
      let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
      if (idb) {
        idb = new IDB(idb);
        idb.upgradeDB(idb.version, 1);
      } fetch(`/order/${order.id}`, { 
        method: 'post',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        },
      }).then(res => res.json())
      .then(({success, result}) => {
        if (success) {
          message.success(`${item.title} has been accepted!`);
          this.dispatch({
            type: 'UPDATE_ORDERS',
            payload: [],
          }).then(refresh);
        } else message.error(result.message);
      }).catch(() => {
        message.error('Network error...try again later!');
      });
    }

    onSuggest = (e) => {
      const { order } = this.props;
      const { location, time, date, item } = this.state;
      let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
      if (idb) {
        idb = new IDB(idb);
        idb.upgradeDB(idb.version, 1);
      } fetch(`/order/${order.id}`, { 
        method: 'put',
        headers: {
          'Content-Type': 'application/json; charset=utf-8'
        }, body: JSON.stringify({
          date,
          time,
          location,
        }),
      }).then(res => res.json())
      .then(({success, result}) => {
        if (success) {
          message.success(
            `${item.title} has been updated!`
          );
        } else message.error(result.message);
      }).catch(() => {
        message.error('Network error...try again later!');
      });
    }

    onLoc = (e) => {
      const { value } = e.target;
      this.setState({ location: value });
    }

    onDate = (e, date) => {
      this.setState({ date });
    }

    onTime = (e, time) => {
      this.setState({ time });
    }

    render () {
        const { item, location, time, date } = this.state;
        const { order, user } = this.props;
        return (
            <Skeleton loading={!item} active>
                <Card className="request" type="inner" title={`${item ? item.title: ''} - ${
                    Number(item ? item.price * order.quant : 0).toFixed(2)
                }`}>
                    <p>Owner: {item ? item.owner : ''}</p>
                    <Input 
                      type='text' 
                      size='large' 
                      placeholder='Dropoff location' 
                      onChange={this.onLoc} 
                      value={location}
                      disabled={order.accepted}
                    />
                    <br/>
                    <br/>
                    <DatePicker 
                      size='large' 
                      onChange={this.onDate} 
                      defaultValue={moment(date)}
                      disabled={order.accepted}
                    />
                    <br/>
                    <br/>
                    <TimePicker 
                      size='large' 
                      format={format} 
                      onChange={this.onTime} 
                      defaultValue={moment(time, format)}
                      disabled={order.accepted}
                    />
                    <br/>
                    <br/>
                    {
                      order && order.accepted && order.ownerId === user.id ? 
                        <Button type='primary' onClick={this.onDone}>Done</Button> :
                        order && order.from !== user.id ? 
                          <Button type='primary' disabled={order.accepted} onClick={this.onAccept}>Accept</Button> :
                          <Button type='primary' disabled={order.accepted} onClick={this.onSuggest}>Suggest</Button>
                    }
                </Card>
            </Skeleton>
        );
    }
}