import React, { Component } from 'react'
import { connect } from 'react-redux'

import { List, Input, Avatar, Skeleton, message } from 'antd'

const { Item } = List
const { Meta } = Item

import { IDB } from '../../utils/idb'

@connect((store) => {
  return {
    cart: store.cart,
  };
})

export default class CartItem extends Component {
  state = { item: null };

  componentDidMount () {
    const { item: { itemId } } = this.props;
    let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
    if (idb) {
      idb = new IDB(idb);
      idb.upgradeDB(idb.version, 1);
    } fetch(`/items/${itemId}`)
    .then(res => res.json())
    .then(({success, result}) => {
      if (!success) return;
      this.setState({ item: result });
    }).catch(() => {
      if (idb) {
        idb.getObject(itemId, 'item', (data, db) => {
          this.setState({ item: data });
        });
      } else message.error('Network error...try again later!');
    });
  }

  updateCart = () => {
    let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
    if (idb) {
      idb = new IDB(idb);
      idb.upgradeDB(idb.version, 1);
    } fetch(`/cart`)
    .then(res => res.json())
    .then(({success, result}) => {
      if (!success) return;
      this.props.dispatch({
        type: 'UPDATE_CART',
        payload: result,
      });
    }).catch(() => {
      if (idb) {
        idb.getAll('cart', (data, db) => {
          this.props.dispatch({
            type: 'UPDATE_CART',
            payload: data,
          });
        });
      } else message.error('Network error...try again later!');
    });
  }

  onUpdate = (e) => {
    const { item } = this.props;
    let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
    if (idb) {
      idb = new IDB(idb);
      idb.upgradeDB(idb.version, 1);
    } fetch(`/cart/${item.id}`, { 
      method: 'put',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }, body: JSON.stringify({
        id: item.id,
        quant: this.state.value,
      }),
    }).then(res => res.json())
    .then(({success, result}) => {
      if (success) {
        message.success(
          `${this.state.item.title} has been updated!`
        );
        
        if (idb) {
          item.quant = this.state.value;
          idb.putObject('cart', item);
        }
      } else message.error(result.message);
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  onRemove = (e) => {
    const { item } = this.props;
    let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
    if (idb) {
      idb = new IDB(idb);
      idb.upgradeDB(idb.version, 1);
    } fetch(`/cart/${item.id}`, { 
      method: 'delete'
    }).then(res => res.json())
    .then(({success, result}) => {
      if (success) {
        this.props.dispatch({
          type: 'UPDATE_CART',
          payload: [],
        });
        message.success(
          `${this.state.item ? this.state.item.title : 'Item'} has been deleted!`
        );
        
        if (idb) {
          idb.deleteObject('cart', item.id);
        } this.updateCart();
      } else message.error(result.message);
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  onChange = (e) => {
    const { value } = e.target;
    this.setState({ value });
  }

  render () {
    const { item, value } = this.state;
    return <Item actions={[
      <a key={`update-${this.props.item.id}`} onClick={this.onUpdate}>update</a>, 
      <a key={`remove-${this.props.item.id}`} onClick={this.onRemove}>x</a>
    ]}>
      <Skeleton avatar title={false} loading={!item} active>
        <Meta
          title={item ? `${item.title} - $${Number(item.price).toFixed(2)}`: ''}
          avatar={<Avatar src={item ? item.image : ''}/>}
          description={
            <div>{
              `Owner: ${item ? item.owner : ''} - ${item ? item.zip : ''}`
            }<br/>{
              `Total: $${
                Number(item ? item.price * (value || this.props.item.quant) : 0).toFixed(2)
              }`
            }</div>
          }
        />
        <Input
          type='number' 
          className='cart-item' 
          placeholder='Quantity' 
          min={1} 
          max={item ? item.quant : 0}
          onChange={this.onChange}
          value={value || this.props.item.quant}
        />
      </Skeleton>
    </Item>;
  }
}
