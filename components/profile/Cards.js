import React, { Component } from 'react'
import { connect } from 'react-redux'
import inputs from '../utils/inputs'
import idb from 'idb'

import { StripeProvider, Elements } from 'react-stripe-elements'

import NewCard from './NewCard'
import Card from './Card'

@connect((store) => {
  return {
    user: store.user,
    cards: store.cards,
    info: store.info,
    current: store.current
  };
})

export default class Cards extends Component {

  componentDidMount() {
    this.update().catch(() => {
      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: 'Network error...try again later!' }
      });

      // TODO: load idb
      const dbPromise = idb.open('oo-db-v1', 1, inputs.upgrade);

      dbPromise.then(db => {
        return db.transaction('cards')
                 .objectStore('cards')
                 .getAll();
      }).then(data => {
        this.props.dispatch({
          type: 'UPDATE_CARDS',
          payload: data
        });
      });
    }).then(() => {
      inputs.removeLoader();
    });
  }

  newCard() {
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: Object.assign({ newCard: true }, this.props.current)
    });
  }

  updateMe(res) {
    if (res) this.props.dispatch({
      type: 'UPDATE_ERROR',
      payload: res.result
    });

    this.update().then(() => {
      this.props.dispatch({
        type: 'UPDATE_CURRENT',
        payload: { profile: true }
      });
    }).catch(() => {
      this.props.dispatch({
        type: 'UPDATE_ERROR',
        payload: { message: 'Network error...try again later!' }
      });
    }).then(() => {
      inputs.removeLoader();
    });
  }

  update() {
    return fetch('/cards')
    .then(res => res.json())
    .then((res) => {
      if (!res.success) {
        console.log(res.result);
        this.props.dispatch({
          type: 'UPDATE_ERROR',
          payload: res.result
        });
        return;
      }

      // TODO: update idb
      this.props.dispatch({
        type: 'UPDATE_CARDS',
        payload: res.result
      });

      const dbPromise = idb.open('oo-db-v1', 1, inputs.upgrade);

      dbPromise.then(db => {
        const tx = db.transaction('cards', 'readwrite');
        res.result.forEach(s => tx.objectStore('cards').put(s));
        return tx.complete;
      });
    });
  }

  addCard(token) {
    this.props.dispatch({
      type: 'UPDATE_INFO',
      payload: Object.assign({ token: token }, this.props.info)
    });
  }

  render() {
    let cards = this.props.cards ? this.props.cards : [];

    if (!cards.length) cards = <p>your wallet is empty!</p>;
    else cards = cards.map((c) => {
      return <Card key={c.id} card={c} update={
        this.updateMe.bind(this)
      } add={ this.addCard.bind(this) }/>;
    });

    let newCard = <a role='button' className='submit' onClick={
      this.newCard.bind(this)
    }>add a card</a>;

    if (this.props.current.newCard)
      newCard = (
        <StripeProvider apiKey='pk_test_fDHCfTp2rFSPP9q9uJpbg4VV'>
          <Elements>
            <NewCard update={ this.updateMe.bind(this) }/>
          </Elements>
        </StripeProvider>
      );

    return (
      <ul className='cards none'>
        {cards}
        <br/>
        {newCard}
      </ul>
    );
  }
}
