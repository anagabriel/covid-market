import React, { Component } from 'react'
import { injectStripe, CardElement } from 'react-stripe-elements'
import inputs from '../utils/inputs'

class NewCard extends Component {

  saveMe() {
    inputs.addLoader();
    this.props.stripe.createToken({ type: 'card' })
    .then(({ token }) => {
      if (!token) this.props.update({
        result: { message: 'Network error...try again later!' }
      });

      let card = { token: token.id };

      fetch('/cards', {
        method: 'POST',
        headers: { "Content-Type": "application/json; charset=utf-8" },
        body: JSON.stringify(card)
      }).then(res => res.json())
      .then((res) => {
        this.props.update(res);
      }).catch(() => {
        this.props.update({
          result: { message: 'Network error...try again later!' }
        });
      });
    });
  }

  render () {
    return (
      <div>
        <CardElement style={{ base: {fontSize: '25px', textTransform: 'lowercase'} }} />
        <a role='button' className='submit' onClick={
          this.saveMe.bind(this)
        }>save card</a>
      </div>
    );
  }
}

export default injectStripe(NewCard);
