import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Card, Empty, Input, Button, message } from 'antd'

import Requests from './Requests'
import Cart from './Cart'

import { IDB } from '../../utils/idb'

@connect((store) => {
  return {
    cart: store.cart,
    user: store.user,
  };
})

export default class Profile extends Component {
  
  onOrder = (e) => {
    e.target.setAttribute('disabled', true);
    const { cart } = this.props;
    let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
    if (idb) {
      idb = new IDB(idb);
      idb.upgradeDB(idb.version, 1);
    } fetch(`/order`, { 
      method: 'post'
    }).then(res => res.json())
    .then(({success, result}) => {
      console.log(success, result);
      if (success) {
        message.success(`Your orders have been placed!`);
        if (idb) {
          cart.forEach(item => {
            idb.deleteObject('cart', item.id);
          });
        } this.props.dispatch({
          type: 'UPDATE_CART',
          payload: [],
        });
      } else message.error(result.message);
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  onSave = (e) => {
    const { user } = this.props;
    const fullname = document.getElementById('fullname');
    const phone = document.getElementById('phone');
    const zip = document.getElementById('zip');

    let errors = 0;
    if (fullname.value == '' || !fullname.value) {
      errors++;
      fullname.classList.add('required');
      message.error('Your name is missing!');
    } else if (fullname.classList.contains('required')) {
      fullname.classList.remove('required');
    } if (zip.value == '' || !zip.value) {
      errors++;
      zip.classList.add('required');
      message.error('Your zip code is missing!');
    } else if (zip.classList.contains('required')) {
      zip.classList.remove('required');
    }
    
    if (errors) return;  
    fetch(`/users`, { 
      method: 'post',
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      }, body: JSON.stringify({
        id: user.id,
        zip: zip.value,
        login: user.login,
        phone: phone.value,
        fullname: fullname.value,
      }),
    }).then(res => res.json())
    .then((res) => {
      if (res.success) {
        message.success(`${res.result.fullname}'s personal info has been updated!`);
        this.props.dispatch({
          type: 'LOGIN',
          payload: res.result,
        });
      } else message.error(res.result.message);
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  render () {
    const { cart, user } = this.props;
    return <Card title={user.fullname}>
      <Card type="inner" title="Cart">
        <Cart/>
        {cart && cart.length ? <Button type="primary" onClick={this.onOrder}>Place Order</Button> : []}
      </Card>
      <br/>
      <Card type="inner" title="Personal Information">
        <Input className='margin-bottom' type='email' value={user.id} disabled/>
        <Input id='fullname' type='text' defaultValue={user.fullname} className='margin-bottom'/>
        <Input
          id='phone'
          type='tel'  
          className='margin-bottom'
          placeholder='(000) 000-0000'
          defaultValue={user.phone || null}
        />
        <Input 
          id='zip'
          max='99999' 
          min='00000'
          type='number'
          placeholder='Zip code: 94042'
          className='margin-bottom' 
          defaultValue={user.zip || null}
        />
        <Button type="primary" onClick={this.onSave}>Save</Button>
      </Card>
      <br/>
      <Card type="inner" title="Requests">
        <div className="requests">
          <Requests/>
        </div>
      </Card>
      {/* <br/>
      <Card type="inner" title="History">
        <Empty />
      </Card> */}
    </Card>;
  }
}
