import React, { Component } from 'react'
import { connect } from 'react-redux'

import { message, Empty } from 'antd'
import Request from './Request'

import { IDB } from '../../utils/idb'

@connect((store) => {
    return {
      user: store.user,
      orders: store.orders,
    };
})

export default class Requests extends Component {

    componentDidMount () {
      this.toFetch();
    }

    toFetch = () => {
      let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
      if (idb) {
        idb = new IDB(idb);
        idb.upgradeDB(idb.version, 1);
      } fetch(`/order`)
      .then(res => res.json())
      .then(({success, result}) => {
        if (!success) return;
        this.props.dispatch({
          type: 'UPDATE_ORDERS',
          payload: result,
        });
      }).catch(() => {
        if (idb) {
          idb.getAll('order', (data, db) => {
            this.props.dispatch({
              type: 'UPDATE_ORDERS',
              payload: data,
            });
          });
        } else message.error('Network error...try again later!');
      });
    }

    render () {
        const { orders } = this.props;
        return orders && orders.length ? 
            orders.map(o => <Request key={o.id} order={o} refresh={this.toFetch}/>) : 
            <Empty/>;
    }
}