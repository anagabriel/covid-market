import React, { Component } from 'react'
import inputs from '../utils/inputs'
import idb from 'idb'

export default class Card extends Component {

  deleteMe() {
    inputs.addLoader();
    fetch(`/cards/${this.props.card.id}`, { method: 'DELETE' })
    .then(res => res.json())
    .then((res) => {
      const dbPromise = idb.open('oo-db-v1', 1, inputs.upgrade);

      dbPromise.then(db => {
        const tx = db.transaction('cards', 'readwrite');
        tx.objectStore('cards').delete(this.props.card.id);
        return tx.complete;
      });

      this.props.update(res);
    }).catch(() => {
      console.log('Network error...');
      this.props.update({
        result: { message: 'Network error...try again later!' }
      });
    });
  }

  selectMe(e) {
    let list = document.querySelectorAll('.cards li');
    for (let l of list) l.classList.remove('active');

    e.target.parentElement.classList.add('active');
    this.props.add(this.props.card.id);
  }

  render () {
    return (
      <li>
        <a onClick={ this.deleteMe.bind(this) }>x</a>
        <p onClick={ this.selectMe.bind(this) }>{
          this.props.card.last4 + ' - ' +
          this.props.card.exp_month + '/' +
          this.props.card.exp_year
        }</p>
      </li>
    );
  }
}
