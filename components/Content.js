import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Empty } from 'antd'

import Catalog from './catalog/Catalog'
import Profile from './profile/Profile'
import Form from './login/Login'

@connect((store) => {
  return {
    page: store.page,
    current: store.current,
  };
})

export default class Content extends Component {
  render() {
    const { page } = this.props;
    let cont = <div className='no-data'><Empty /></div>;

    if (page === 'catalog')
      cont = <Catalog />;
    else if (page === 'login' || page === 'reset')
      cont = <div className='center-content'><Form /></div>;
    else if (page === 'profile')
      cont = <Profile />;

    return <main className='site-layout'>{ cont }</main>;
  }
}
