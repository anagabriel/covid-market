import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Menu, message } from 'antd'

import { truncateName } from '../../utils/std'
import { IDB } from '../../utils/idb'

@connect((store) => {
  return {
    user: store.user,
    page: store.page,
    current: store.current,
  };
})

export default class TopBar extends Component {

  componentDidMount () {
    fetch(`/creds`)
    .then(res => res.json())
    .then(({success, result}) => {
      if (!success) return;
      this.props.dispatch({
        type: 'LOGIN',
        payload: result,
      });
    }).catch(() => {
      message.error('Network error...try again later!');
    });
  }

  onLogin = (e) => {
    const { user } = this.props;
    const tmp = this.props.current;
    
    if (tmp.indexOf(e.key) < 0 && !user.fullname) 
      this.props.dispatch({
        type: 'GOTO_LOGIN',
        payload: true,
      });
    else if (tmp.indexOf(e.key) < 0 && user.fullname) 
      this.props.dispatch({
        type: 'GOTO_PROFILE',
        payload: true,
      });
    else this.props.dispatch({
      type: 'GOTO_CATALOG',
      payload: true,
    });

    this.onClick(e);
  }

  onClick = (e) => {
    const tmp = this.props.current.slice();
    
    if (tmp.indexOf(e.key) < 0) tmp.push(e.key);
    else tmp.splice(tmp.indexOf(e.key), 1);
    if (e.key !== '6' && tmp.indexOf('6') > -1) {
      tmp.splice(tmp.indexOf('6'), 1);
    }
    
    this.updateItems(tmp);
    this.props.dispatch({
      type: 'UPDATE_CURRENT',
      payload: tmp,
    });

    if (e.key !== '6') this.props.dispatch({
      type: 'GOTO_CATALOG',
      payload: true,
    });
  }

  updateItems = (current) => {
    let idb = indexedDB || mozIndexedDB || webkitIndexedDB || msIndexedDB;
    if (idb) {
      idb = new IDB(idb);
      idb.upgradeDB(idb.version, 1);
    } fetch(`/items${
      current && current.length ? `?category=${current.map(c => `cat-${c}`).join(';')}` : ''
    }`).then(res => res.json()).then(({success, result}) => {
      if (!success) {
        message.error(result.message);
        return;
      }

      this.props.dispatch({
        type: 'UPDATE_ITEMS',
        payload: result,
      });

      if (idb) {
        result.forEach(item => {
          idb.putObject('items', item);
        });
      }
    }).catch(() => {
      if (idb) {
        idb.getAll('items', (data, db) => {
          this.props.dispatch({
            type: 'UPDATE_ITEMS',
            payload: data,
          });
        });
      } else message.error('Network error...try again later!');
    });
  }

  render () {
    const { current, user } = this.props;
    const name = truncateName(user.fullname);

    return (
      <Menu mode="horizontal" selectedKeys={current}>
        <Menu.Item key="1" onClick={this.onClick}>TP &amp; Wipes</Menu.Item>
        <Menu.Item key="2" onClick={this.onClick}>Paper Towels</Menu.Item>
        <Menu.Item key="3" onClick={this.onClick}>Tissues</Menu.Item>
        <Menu.Item key="4" onClick={this.onClick}>Gloves</Menu.Item>
        <Menu.Item key="5" onClick={this.onClick}>Masks</Menu.Item>
        <Menu.Item className='login-button' key="6" onClick={this.onLogin}>{name}</Menu.Item>
      </Menu>
    );
  }
}
