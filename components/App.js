import React, { Component } from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import store from '../store'

import { Layout } from 'antd'

import TopBar from './bar/TopBar'
import Content from './Content'

const { Header } = Layout

class App extends Component {
  render() {
    return (
      <Layout>
        <Header>
          <TopBar/>
        </Header>
        <Content />
    </Layout>
    );
  }
}

ReactDOM.render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('app'));
