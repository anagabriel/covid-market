export default function cartReducer(state=[], action) {
  switch (action.type) {
    case 'ADD_TO_CART': {
      const tmp = state.slice();
      tmp.push(action.payload);
      state = tmp;
      break;
    } case 'UPDATE_CART':
      state = action.payload;
      break;
  } return state;
};
