export default function userReducer(state={}, action) {
  switch (action.type) {
    case 'LOGIN':
      state = action.payload;
      break;
    case 'LOGOUT':
      state = { fullname: null, id: null, phone: null };
      break;
    case 'NAME':
      state = {
        ...state,
        fullname: action.payload
      };
      break;
  } return state;
};
