export default function navReducer(state='catalog', action){
  switch (action.type) {
    case 'GOTO_LOGIN':
      state = 'login';
      break;
    case 'GOTO_RESET':
      state = 'reset';
      break;
    case 'GOTO_CATALOG':
      state = 'catalog';
      break;
    case 'GOTO_PROFILE':
      state = 'profile';
      break;
  } return state;
};
