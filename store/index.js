import { combineReducers, createStore } from 'redux'

import cart from './cart'
// import cards from './cards'
import current from './current'
import items from './items'
import user from './user'
import error from './error'
import page from './page'
import orders from './orders'

export default createStore(combineReducers({
  user,
  // cards,
  cart,
  items,
  current,
  error,
  page,
  orders,
}));
