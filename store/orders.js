export default function ordersReducer(state=[], action) {
    switch (action.type) {
      case 'UPDATE_ORDERS':
        state = action.payload;
        break;
      case 'UPDATE_ORDER_ITEM': {
        const { index, item } = action.payload;
        const tmp = state.slice();
        tmp[index].item = item;
        state = tmp;
      } break;
    } return state;
  };
  