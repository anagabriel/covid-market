function udpateidb(idb, request, json) {
  let blob = new Blob(
    [JSON.stringify(json)],
    {type : 'application/json'}
  );

  if (!idb) return new Respone(blob, {"status" : 504});

  idb.putObject('todo', serialize(request));
  idb.putObject('items', json);

  return new Respone(blob, {"status" : 200});
}

function deleteidb(idb, request, split) {
  let blob = new Blob(
    [JSON.stringify({deleted: false})],
    {type : 'application/json'}
  );

  if (!idb) return new Respone(blob, {"status" : 504});

  idb.putObject('todo', serialize(request));
  idb.getAll('items', (all) => {
    let obj = all[parseInt(split[split.length-1])];
    idb.deleteObject('items', obj.name);
  });

  return new Respone(blob, {"status" : 200});
}

function update(idb, navigator, request) {
  return request.clone().json((json) => {
    if (navigator.onLine)
      return fetch(request).then((res) => {
        return res.clone().json((resJ) => {
          console.log(resJ);
          idb.putObject('items', resJ);
          return res;
        });
      }).catch((error) => {
        console.log('network failure', error);
        return updateidb(request, json);
      });

      return updateidb(request, json);
  });
}

function remove(idb, navigator, request, url) {
  let split = url.pathname.split('/');

  if (navigator.onLine)
    return fetch(request).then((res) => {
      idb.getAll('items', (all) => {
        let obj = all[parseInt(split[split.length-1])];
        idb.deleteObject('items', obj.name);
      });
      return res;
    }).catch((error) => {
      console.log('network failure', error);
      return deleteidb(request, split);
    });

  return deleteidb(request, split);
}
