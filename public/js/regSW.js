'use strict';

navigator.serviceWorker.register('serviceworker.js').then(function (reg) {
  console.log('Service Worker has been registered!');
}).catch(function (err) {
  console.error('Oops, something went wrong.', err);
});
