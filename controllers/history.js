const table = require('../utils/table');
const name = table.set('cm-history').TableName;

module.exports = {
  table: (callback) => {
    table.table(name, (err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback) => {
    table.all(name, (err, data) => {
      callback(err, data);
      return;
    });
  },

  get: (id, callback) => {
    table.get(name, id, (err, data) => {
      callback(err, data);
      return;
    });
  },

  find: (params, callback, last) => {
    table.find(name, params, (err, data, l) => {
      if (l) data = {
        data,
        last: l
      }; callback(err, data);
      return;
    }, last);
  },

  findById: (id, callback) => {
    table.get(name, id, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findByUserId: (userId, callback) => {
    table.find(name, {'userId': userId}, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findByOwner: (owner, callback) => {
    table.find(name, {'owner': owner}, (err, data) => {
      callback(err, data);
      return;
    });
  },

  findByItemId: (id, callback) => {
    table.find(name, {'itemId': id}, (err, data) => {
      callback(err, data);
      return;
    });
  },

  totalByUserId: (userId, callback) => {
    table.find(name, {'userId': userId}, (err, content) => {
      if (err) {
        callback(err, null);
        return;
      }

      let count = 0, obj = {
        cart: content,
        total: 0
      };

      content.forEach((i) => {
        item.findById(i.itemId, (err, result) => {
          if (err || !result) console.error(err || result);
          obj.total += (parseInt(i.quan) * parseInt(result.price));
          ++count;
          if (count == content.length) callback(null, obj);
        });
      });
    });
  },

  create: (params, callback) => {
    let check = {
      userId: params.userId,
      itemId: params.itemId
    };

    table.find(name, check, (err, content) => {
      if (err || content.length == 0 || !content) {
        params.quan = !params.quan ? 1 : params.quan;
        table.create(name, params, (err, content) => {
          callback(err, content);
          return;
        });
        return;
      }
      if (params.quan) content[0].quan = params.quan;
      else ++content[0].quan;
      table.update(name, content[0].id, content[0], (err, result) => {
        if (err) return null;
        callback(null, content[0]);
      });
    });
  },

  update: (id, params, callback) => {
    table.get(name, id, (err, data) => {
      if (!data) {
        callback({
          message: 'cart does not exist!'
        }, null);
        return;
      } table.update(name, id, params, (e, d) => {
        callback(e, d);
        return;
      });
    });
  },

  delete: (id, callback) => {
    table.get(name, id, (err, data) => {
      if (!data) {
        callback({
          message: 'cart does not exist!'
        }, null);
        return;
      } table.delete(name, id, (e, d) => {
        callback(e, data);
        return;
      });
    });
  }
}
