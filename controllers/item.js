const table = require('../utils/table');
const name = table.set('cm-item').TableName;

module.exports = {

  table: (callback) => {
    table.table(name, (err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback, last) => {
    table.all(name, (err, data, l) => {
      if (l) data = {
        data,
        last: l
      }; callback(err, data);
      return;
    }, last);
  },

  count: (callback) => {
    table.count(name, (err, data) => {
      callback(err, data);
      return;
    });
  },

  get: (id, callback) => {
    table.get(name, id, (err, data) => {
      callback(err, data);
      return;
    });
  },

  find: (params, callback) => {
    table.find(name, params, (err, data) => {
      callback(err, data);
      return;
    });
  },

  search: (key, array, callback) => {
    table.search(name, key, array, (err, data) => {
      callback(err, data);
      return;
    });
  },

  create: (params, callback) => {
    table.get(name, params.id, (err, data) => {
      if (data) {
        callback({
          message: 'item already exists!'
        }, null);
        return;
      } table.create(name, params, (e, d) => {
        callback(e, d);
        return;
      });
    });
  },

  update: (id, params, callback) => {
    table.get(name, id, (err, data) => {
      if (!data) {
        callback({
          message: 'item does not exist!'
        }, null);
        return;
      } table.update(name, id, params, (e, d) => {
          callback(e, d);
          return;
      });
    });
  },

  delete: (id, callback) => {
    table.delete(name, id, (err) => {
      callback(err, null);
      return;
    });
  }

}