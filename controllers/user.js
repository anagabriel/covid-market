const table = require('../utils/table');
const name = table.set('cm-user', null, 10).TableName;

module.exports = {

  table: (callback) => {
    table.table(name, (err, data) => {
      callback(err, data);
      return;
    });
  },

  all: (callback) => {
    table.all(name, (err, data) => {
      for (let i = 0; i < data.length; i++) {
        delete data[i].code;
        delete data[i].stamp;
        delete data[i].password;
        delete data[i].reset_stamp;
      } callback(err, data);
      return;
    });
  },

  get: (id, callback) => {
    table.get(name, id, (err, data) => {
      delete data.code;
      delete data.stamp;
      delete data.password;
      delete data.reset_stamp;
      callback(err, data);
      return;
    });
  },

  find: (params, callback) => {
    table.find(name, params, (err, data) => {
      delete data.code;
      delete data.stamp;
      delete data.password;
      delete data.reset_stamp;
      callback(err, data);
      return;
    });
  },

  create: (params, callback) => {
    table.find(name, {'id': params['id']}, (err, data) => {
      if (data.length) {
        callback({
          message: 'user already exists!'
        }, null);
        return;
      } table.create(name, params, (e, d) => {
        callback(e, d);
        return;
      });
    });
  },

  update: (id, params, callback) => {
    table.find(name, {id}, (err, data) => {
      if (!data.length) {
        callback({
          message: 'user does not exist!'
        }, null);
        return;
      } table.update(name, id, params, (e, d) => {
          callback(e, d);
          return;
      });
    });
  },

  verify: (id, params, callback) => {
    table.get(name, id, (err, data) => {
      if (data && data.password && data.password === params.password) {

        delete data.code;
        delete data.stamp;
        delete data.password;
        delete params.password;
        delete data.reset_stamp;

        table.update(name, id, params, (e, d) => {
          callback(e, d);
          return;
        });
        return;
      } else if (!data)
        err = {
          message: 'incorrect email!'
        }, data = null;
      else err = {
        message: 'incorrect password!'
      }, data = null;
      callback(err, data);
      return;
    });
  },

  check: (id, params, callback) => {
    table.get(name, id, (err, data) => {
      if (data && data.login && data.login === params.login) {
        const now = new Date();
        const logged = new Date(data.stamp);

        if (now.getFullYear() !== logged.getFullYear())
          err = {
            message: 'your session expired!'
          }, data = null;
        else if (
          new Date(
            now.getFullYear(),
            logged.getMonth(),
            logged.getDate() + 1
          ) < now
        ) err = {
          message: 'your session expired!'
        }, data = null;
        else {
          delete data.code;
          delete data.stamp;
          delete data.password;
          delete data.reset_stamp;
        }
      } else if (!data)
        err = {
          message: 'incorrect email!'
        }, data = null;
      else err = {
        message: 'incorrect session!'
      }, data = null;
      callback(err, data);
      return;
    });
  },

  reset: (id, params, callback) => {
    table.get(name, id, (err, data) => {
      if (data && data.reset && data.reset === params.reset) {
        const now = new Date();
        const logged = new Date(data.stamp);

        if (now.getFullYear() !== logged.getFullYear())
          err = {
            message: 'your reset code has expired!'
          }, data = null;
        else if (
          new Date(
            now.getFullYear(),
            logged.getMonth(),
            logged.getDate() + 1
          ) < now
        ) err = {
          message: 'your reset code has expired!'
        }, data = null;
        else {
          table.update(name, id, params, (e, d) => {
            table.remove(name, {
              id,
              reset: params.reset
            }, (r, a) => {
              if (d.password) delete d.password;
              callback(e, d);
              return;
            });
            return;
          });
          return;
        }
      } else if (!data)
        err = {
          message: 'incorrect email!'
        }, data = null;
      else err = {
        message: 'incorrect reset code!'
      }, data = null;
      callback(err, data);
      return;
    });
  },

  remove: (params, callback) => {
    table.remove(name, params, (err, data) => {
      callback(err, data);
      return;
    })
  },

  delete: (id, callback) => {
    table.delete(name, id, (err) => {
      callback(err, null);
      return;
    });
  }

}

