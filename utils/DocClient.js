var aws = require('aws-sdk');

aws.config.update({
    accessKeyId: '',
    secretAccessKey: '',
    region: ''
});

var docClient = new aws.DynamoDB.DocumentClient();

module.exports = docClient;
