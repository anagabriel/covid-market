var aws = require('aws-sdk');

aws.config.update({
    accessKeyId: '',
    secretAccessKey: '',
    region: ''
});

var s3 = new aws.S3();

module.exports = s3;