var aws = require('aws-sdk');

aws.config.update({
    accessKeyId: '',
    secretAccessKey: '',
    region: ''
});

var ses = new aws.SES();

module.exports = ses;