const std = require('./std');
const users = require('../controllers/user');

function admin (req, res, next) {
  const u = {
    id: req.cookies['id'],
    login: req.cookies['login']
  };

  if (u.id != 'ana.gabriel2012@gmail.com') {
    res.json(
      std.response({message: 'Admin needs to login!'})
    );
    return;
  }

  users.check(u.id, u, (err, data) => {
    if (err) {
      res.json(
        std.response(err)
      );
      return;
    } return next();
  });
  return;
}

function user (req, res, next) {
  const u = {
    id: req.cookies['id'],
    login: req.cookies['login']
  };

  if (req.originalUrl === '/users' && u.id !== req.body.id) {
    res.json(
      std.response({message: `you don't own this account!`})
    );
    return;
  } 

  users.check(u.id, u, (err, data) => {
    if (err) {
      res.json(
        std.response(err)
      );
      return;
    } next();
    return;
  });
  
  return;
}

module.exports = {admin, user};
