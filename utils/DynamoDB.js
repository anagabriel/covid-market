var aws = require('aws-sdk');

aws.config.update({
    accessKeyId: '',
    secretAccessKey: '',
    region: ''
});

var dynamodb = new aws.DynamoDB();

module.exports = dynamodb;