function response (err, data) {
  if (err) {
    console.log(err);
    return {
      success: false,
      result: err
    };
  } return {
    success: true,
    result: data
  };
}

function string (length) {
  let i = 0,
      str = '',
      p = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
          'abcdefghijklmnopqrstuvwxyz' +
          '0123456789';

  for (; i < length; i++,
     str += p[
       Math.floor(Math.random() * p.length)
     ]
   );

  return str;
}

function cleanUpData (data) {
  for (let key in data) {
    if (!data[key]) continue;
    data[key] = data[key].toString().trim();
  } return data;
}

function isUserValid (user) {
  let result = { success: false };

  user = cleanUpData(user);
  if(!user.email || !isEmailValid(user.email)) {
    result.message = 'Please, enter your email!';
  } else if(!user.password || !isPasswordValid(user.password)) {
    result.message = 'Please, enter a valid password!';
  } else if (user.password !== user.confirm) {
    result.message = 'Passwords did not match!';
  } else {
    result.success = true;
    result.user = {
      id: user.email,
      password: user.password
    };
  }

  return result;
}

function isEmailValid (email) {
  return email.indexOf('@') < email.lastIndexOf('.') &&
         email.indexOf('@') !== -1;
}

function isPasswordValid (password) {
  const regex = /^[\.\?\,\-\!0-9a-zA-Z]+$/;
  return password.length >= 12 && password.match(regex);
}

function isAlphaNumeric (word) {
  const regex = /^[0-9a-zA-Z]+$/;
  return word.match(regex);
}

function grabURL (req) {
  const host = req.headers.host;
  const pre = host.indexOf('localhost') !== -1 ? 'http://' : 'https://';
  return { host, pre };
}

function truncateName(name) {
  let usr = name;

  if (usr && usr.length) {
    if (usr.indexOf(' ') !== -1) usr = usr.substr(0, usr.indexOf(' '));
    if (usr.indexOf('-') !== -1) usr = usr.substr(0, usr.indexOf('-'));
    if (usr.indexOf('.') !== -1) usr = usr.substr(0, usr.indexOf('.'));
    if (usr.indexOf('_') !== -1) usr = usr.substr(0, usr.indexOf('_'));
    if (usr.length > 6) usr = usr.substr(0, 3) + '...';
  } else usr = 'Login';

  return usr;
}

module.exports = {
  string,
  grabURL,
  response,
  cleanUpData,
  isUserValid,
  isEmailValid,
  truncateName,
  isAlphaNumeric,
}
