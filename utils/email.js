const nodemailer = require('nodemailer');
const ses = require('./SES');

const transporter = nodemailer.createTransport({ SES: ses });

function sendEmail(email, msg) {
  let mailOptions = {
    from: 'no-reply@covidmarket.app',
    to: email,
    subject: 'Message from Covid Market',
    text: msg
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.error(error);
    } else {
      console.log(`Email sent: ${info.response}`);
    }
  });
}

function resetPassword(email, key) {
  let mailOptions = {
    from: 'no-reply@covidmarket.app',
    to: email,
    subject: 'Reset Password',
    text: `
Hi,

Your authentication code is: ${key}
Once you've logged in, you'll be prompted for a new password.

- Ana-Gabriel
`};

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.error(error);
    } else {
      console.log(`Email sent: ${info.response}`);
    }
  });
}

module.exports = { sendEmail, resetPassword };
