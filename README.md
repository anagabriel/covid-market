# Covid Market

## Requirements
1. ask user to allow location tracking in browser
2. send an invitation to meetup and pay through the app
3. Stripe handles all transactions
4. allow user to prep cards before the transaction, so they may receive the products
5. each member verifies transaction before it is processed
6. products are available by location