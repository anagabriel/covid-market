const express = require('express');
const router = express.Router();
const crypto = require('crypto');
// const stripe = require('stripe')('sk_test_w35xjDH2lovqN1Qskve5Rp5P');

const user = require('../controllers/user');

const std = require('../utils/std');
const val = require('../utils/val');
const email = require('../utils/email');

router.put('/', (req, res, next) => {
  const valid = std.isUserValid(req.body); 
  if (!valid.success) {
    res.json({
      success: false,
      result: { message: valid.message },
    });
    return;
  }

  const usr = valid.user;
  // stripe.customers.create({ email: req.body.email.trim() }, (errs, customer) => {
  //   if (errs) {
  //     console.error(errs);
  //     res.json({
  //       success: false,
  //       result: { message: 'Stripe account not created!' }
  //     });
  //     return;
  //   }

    crypto.pbkdf2(usr.password, 'salt', 100000, 64, 'sha512', (err, derivedKey) => {
      if (err) {
        res.json({
          success: false,
          result: err
        });
        return;
      }

      usr.stamp = new Date().toString();
      usr.password = derivedKey.toString('hex');
      usr.fullname = std.truncateName(usr.id);
      usr.login = std.string(32);
      
      user.create(usr, (er, result) => {
        if (er) {
          console.error(er);
          res.json({
            success: false,
            result: er
          });
          return;
        }

        res.cookie('id', usr.id);
        res.cookie('login', usr.login);
        // res.cookie('stripeId', usr.stripeId);

        email.sendEmail(usr.id, `
          ${usr.id.substring(0, usr.id.indexOf('@'))},

          Welcome, to Covid Market!  
          Covid Market is a place where you can sell your spare supplies to others in need of it.

          Once someone is interested in your products, they will send you a request.
          The request will contain a date and time for you to drop-off the supplies, 
          so you may appropriately follow social distancing.
          
          You will receive an email regarding the request which you can either accept or suggest another time.
          
          As soon as you and your customer have decided on a time for the drop-off, 
          you will receive their contact information to request money from them.

          If you have COVID-19, please, do not use this app! 
          We want to help prevent the spread of this disease.

          Options you should consider, but are not limited to:

          - Venmo
          - Zelle (most mainstream banks have this option)
          - Paypal

          Make sure to mark your transaction as completed, once your customer has paid you.
          Thank you, for your support in making this happen!

          Sincerely,
          Covid Market
        `);

        res.json({
          success: true,
          result: {
            id: usr.id,
            login: usr.login,
            fullname: usr.fullname,
          }
        });
        return;
      });
    });
  // });
});

router.post('/', val.user, (req, res, next) => {
  if (!req.cookies['login'] || req.cookies['login'] == '') {
    res.json({
      success: false,
      result: { message: 'You have to login!' }
    });
    return;
  }

  const u = {
    id: req.cookies['id'],
    login: req.cookies['login'],
  };

  if (req.body.zip) u.zip = req.body.zip.trim();
  if (req.body.phone) u.phone = req.body.phone.trim();
  if (req.body.fullname) u.fullname = req.body.fullname.trim();

  user.update(u.id, u, (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.cookie('id', u.id);
    if (u.fullname) res.cookie('fullname', u.fullname);

    res.json({
      success: true,
      result: {
        id: u.id,
        zip: u.zip,
        phone: u.phone,
        login: u.login,
        fullname: u.fullname,
      }
    });
    return;
  });
});

router.post('/login', (req, res, next) => {
  crypto.pbkdf2(req.body.password.trim(), 'salt', 100000, 64, 'sha512', (err, derivedKey) => {
    if (err) {
      res.json({
        success: false,
        result: err
      });
      return;
    }

    const usr = {
      login: std.string(32),
      id: req.body.email.trim(),
      stamp: new Date().toString(),
      password: derivedKey.toString('hex'),
    };

    user.verify(usr.id, usr, (er, result) => {
      if (er) {
        console.error(er);
        res.json({
          success: false,
          result: er
        });
        return;
      }

      res.cookie('id', usr.id);
      res.cookie('login', usr.login);
      // res.cookie('stripeId', usr.stripeId);

      res.json({
        success: true,
        result: {
          id: usr.id,
          login: usr.login,
          fullname: result.fullname,
          phone: result.phone,
          zip: result.zip,
        }
      });
      return;
    });
  });
});

router.delete('/:id', (req, res, next) => {
  if (!req.cookies['login'] || req.cookies['login'] == '') {
    res.json({
      success: false,
      result: { message: 'You have to login!' }
    });
    return;
  }

  user.delete(req.params.id.trim(), (err, result) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.cookie('id', '');
    res.cookie('login', '');
    res.cookie('fullname', '');

    res.json({
      success: true,
      result: { message: 'Your account has been deleted!' }
    });
    return;
  });
});

module.exports = router;
