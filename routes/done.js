const express = require('express');
const router = express.Router();

const history = require('../controllers/history');
const order = require('../controllers/order');

const std = require('../utils/std');
const val = require('../utils/val');

router.post('/:id', val.user, (req, res, next) => {
  if (!req.cookies['id'] || req.cookies['id'] == '') {
    res.json({
      success: false,
      result: { message: 'You have to login!' }
    });
    return;
  }

  order.get(req.params.id, (err, result) => {
    if (err) {
      res.json(std.response(err));
      return;
    } history.create(result, (er, re) => {
      if (er) {
        res.json(std.response(er));
        return;
      } order.delete(req.params.id, (e, r) => {
        res.json(std.response(e, r));
        return;
      });
    });
  });
});

module.exports = router;