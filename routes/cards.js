let express = require('express');
let router = express.Router();
let stripe = require('stripe')('sk_test_w35xjDH2lovqN1Qskve5Rp5P');
let user = require('../controllers/user');

router.get('/', (req, res) => {
  if (!req.cookies['stripeId'] || req.cookies['stripeId'] === '') {
    res.json({
      success: false,
      result: { message: 'You need to login!' }
    });
    return;
  }

  // get all payment options from stripe using cookies
  stripe.customers.listCards(req.cookies['stripeId'], (err, cards) => {
    console.error(err);
    if (err) {
      res.json({
        success: false,
        result: err
      });
      return;
    }

    res.json({
     success: true,
     result: cards.data
    });
    return;
  });
});

router.post('/', (req, res) => {
  if (!req.cookies['stripeId'] || req.cookies['stripeId'] === '') {
    res.json({
      success: false,
      result: { message: 'You need to login!' }
    });
    return;
  }

  stripe.customers.createSource(req.cookies['stripeId'],
    { source: req.body.token }, (err, card) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: { message: 'Your card could not be added! :(' }
      });
      return;
    }

    res.json({
     success: true,
     result: { message: 'Your card was added! :D' }
    });
    return;
  });
});

router.delete('/:id', (req, res) => {
  if (!req.cookies['stripeId'] || req.cookies['stripeId'] === '') {
    res.json({
      success: false,
      result: { message: 'You need to login!' }
    });
    return;
  }

  stripe.customers.deleteCard(req.cookies['stripeId'], req.params.id,
  (err, confirmation) => {
    if (err) {
      console.error(err);
      res.json({
        success: false,
        result: { message: 'Your card could not be deleted! :/' }
      });
      return;
    }

    res.json({
      success: true,
      result: { message: 'Your card was deleted!' }
    });
    return;
  });
});

module.exports = router;
