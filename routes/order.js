const express = require('express');
const router = express.Router();
// let stripe = require('stripe')('sk_test_w35xjDH2lovqN1Qskve5Rp5P');

const cart = require('../controllers/cart');
const item = require('../controllers/item');
const order = require('../controllers/order');

const std = require('../utils/std');
const val = require('../utils/val');

/* GET order listing. */
router.get('/', (req, res, next) => {
  if (!req.cookies['id'] || req.cookies['id'] == '') {
    res.json({
      success: false,
      result: { message: 'You have to login!' }
    });
    return;
  }

  order.findByUserId(req.cookies['id'], (err, result) => {
    res.json(
        std.response(err, result)
    );
    return;
  });
});

router.post('/', val.user, (req, res, next) => {
    cart.findByUserId(req.cookies['id'], (err, result) => {
        if (err) {
            res.json(std.response(err));
            return;
        } placeOrder(result, (message) => {
            emptyCart(result, () => {
                res.json(
                    std.response(err, message)
                );
                return;
            });
            return;
        });
        return;
    });
});

router.put('/:id', val.user, (req, res, next) => {
    const date = {
        accepted: false,
        date: req.body.date,
        time: req.body.time,
        from: req.cookies['id'],
        location: req.body.location,
    };

    order.get(req.params.id, (er, re) => {
        if (er || !re) {
            res.json(std.response(er || {
                message: 'Request does not exist!',
            }));
            return;
        } else if (!re.accepted && re.ownerId === req.cookies['id'] || re.userId === req.cookies['id']) {
            order.update(req.params.id, date, (err, result) => {
                res.json(
                    std.response(err, result)
                ); 
                return;
            });
        } else {
            res.json(std.response(er || {
                message: 'You does not own this data! Go away.',
            }));
        } return;
    });
});

router.post('/:id', val.user, (req, res, next) => {
    const date = { accepted: true };
    order.get(req.params.id, (er, re) => {
        if (er || !re) {
            res.json(std.response(er || {
                message: 'Request does not exist!',
            }));
            return;
        } else if (re.ownerId === req.cookies['id'] || re.userId === req.cookies['id']) {
            order.update(req.params.id, date, (err, result) => {
                res.json(
                    std.response(err, result)
                ); 
                return;
            });
        } else {
            res.json(std.response(er || {
                message: 'You does not own this data! Go away.',
            }));
        } return;
    });
});

function placeOrder(items, cb) {
  let count = items.length;
  items.forEach((i) => {
    item.get(i.itemId, (e, r) => {
        const q = Number(r.quant) - Number(i.quant);
        if (q < 0) return;
        item.update(i.itemId, {quant: q}, (er, re) => {
            if (er) return;
            i.ownerId = r.owner;
            order.create(i, (err, result) => {
                if (err) console.log(err);
                if (!--count) cb({ message: 'Your order has been placed!' });
            });
        });
    });
  });
}

function emptyCart(items, cb) {
  let count = items.length;
  items.forEach((i) => {
    cart.delete(i.id, (e, r) => console.log(i.id));
    if (!--count) cb();
  });
}

module.exports = router;