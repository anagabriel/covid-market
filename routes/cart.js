const express = require('express');
const router = express.Router();
// let stripe = require('stripe')('sk_test_w35xjDH2lovqN1Qskve5Rp5P');

const cart = require('../controllers/cart');
const std = require('../utils/std');

/* GET cart listing. */
router.get('/', (req, res) => {
  if (!req.cookies['id'] || req.cookies['id'] == '') {
    res.json({
      success: false,
      result: { message: 'You have to login!' }
    });
    return;
  }

  cart.findByUserId(req.cookies['id'], (err, result) => {
    res.json(
      std.response(err, result)
    );
    return;
  });
});

router.put('/', (req, res) => {
  if (!req.cookies['id'] || req.cookies['id'] == '') {
    res.json({
      success: false,
      result: { message: 'You have to login!' }
    });
    return;
  }

  let item = {
    itemId: req.body.itemId,
    userId: req.cookies['id']
  };

  if (req.body.quant) 
    item.quant = parseInt(req.body.quant);
  item.id = std.string(10);
  cart.create(item, (err, result) => {
    res.json(
      std.response(err, result)
    );
    return;
  });
});

router.put('/:id', (req, res) => {
  if (!req.cookies['id'] || req.cookies['id'] == '') {
    res.json({
      success: false,
      result: { message: 'You have to login!' }
    });
    return;
  }

  const item = {
    id: req.params.id,
    quant: parseInt(req.body.quant)
  };

  cart.get(req.params.id, (er, re) => {
    if (er || re.userId !== req.cookies['id']) {
      res.json(er || {
        message: `You don't own this cart item!`,
      });
      return;
    } cart.update(item.id, item, (err, result) => {
      if (err) {
        console.error(err);
        res.json({
          success: false,
          result: err
        });
        return;
      }

      res.json({
        success: true,
        result: { message: 'Your cart was updated!' }
      });
      return;
    });
  });
});

router.delete('/:id', (req, res) => {
  if (!req.cookies['id'] || req.cookies['id'] == '') {
    res.json({
      success: false,
      result: { message: "You're not logged in!" }
    });
    return;
  }

  cart.get(req.params.id, (er, re) => {
    if (er || re.userId !== req.cookies['id']) {
      res.json(er || {
        message: `You don't own this cart item!`,
      });
      return;
    } cart.delete(req.params.id, (err, result) => {
      if (err) {
        console.error(err);
        res.json({
          success: false,
          result: err
        });
        return;
      }
  
      res.json({
        success: true,
        result: { message: 'Your cart was updated!' }
      });
      return;
    });
  });
});

module.exports = router;
