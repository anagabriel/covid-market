const express = require('express');
const router = express.Router();

const item = require('../controllers/item');
const cart = require('../controllers/cart');

const std = require('../utils/std');
const val = require('../utils/val');
const s3 = require('../utils/S3');

const multerS3 = require('multer-s3');
const multer = require('multer');

const bucket = 'covid-market-images';

const upload = multer({
  storage: multerS3({
    s3,
    bucket,
    acl: 'public-read',
    metadata: (req, file, cb) => {
      cb(null, {fieldName: file.fieldname});
    },
    key: (req, file, cb) => {
      cb(null, Date.now().toString() + '-' + file.originalname)
    }
  })
});

router.get('/', (req, res) => {
  if (req.query.zip) {
    item.find({zip: req.query.zip}, (err, result) => {
      res.json(
        std.response(err, result)
      );
      return;
    });
    return;
  } else if (req.query.owner) {
    item.find({owner: req.query.owner}, (err, result) => {
      res.json(
        std.response(err, result)
      );
      return;
    });
    return;
  } else if (req.query.category) {
    const cats = req.query.category.split(';').filter(c => c);
    item.search('category', cats, (err, result) => {
      res.json(
        std.response(err, result)
      );
      return;
    });
    return;
  }

  item.all((err, result) => {
    res.json(
      std.response(err, result)
    );
    return;
  });
});

router.get('/:id', (req, res, next) => {
  item.get(req.params.id, (err, result) => {
    if (!result) {
      res.json(
        std.response({ 
          message: 'Item does not exist!' 
        })
      );
      return;
    }

    res.json(std.response(err, result));
    return;
  });
});

router.post('/image', val.user, upload.single('avatar'), (req, res, next) => {
  res.json(
    std.response(null, {
      url: req.file.location,
      name: req.file.key
    })
  );
});

router.put('/', val.user, (req, res, next) => {
  req.body.id = std.string(10);
  req.body.owner = req.cookies['id'];
  
  item.create(req.body, (err, result) => {
    res.json(
      std.response(err, result)
    );
    return;
  });
});

// TODO: check if item belongs to cookie person
// router.post('/:id', val.user, (req, res, next) => {
//   req.body.id = req.params.id;
//   item.update(req.body, (err, result) => {
//     res.json(
//       std.response(err, result)
//     );
//     return;
//   });
// });

// TODO: check if item belongs to cookie person
// router.delete('/:id', val.user, fromCarts, (req, res, next) => {
//   item.delete(req.params.id, (err, result) => {
//     if (err) {
//       res.json(std.response(err));
//       return;
//     }

//     let key = result.src.split('/');
//     key = key[key.length-1];

//     deleteImage(key);
//     res.json({
//       success: true,
//       result: { message: `${result.iname} has been deleted!` }
//     });
//   });
// });

function deleteImage(key) {
  let params = {
   Bucket: bucket,
   Key: key
  };

  s3.deleteObject(params, (err, data) => {
    if (err) console.log(err, err.stack);
    else console.log(data);
  });
}

function fromCarts(req, res, next) {
  cart.findByItemId(req.params.id, (err, result) => {
    if (err) {
      res.json(std.response(err));
      return;
    }

    result.forEach((c) => {
      cart.delete(c.id, (e, r) => {
        console.log(e);
        return;
      });
    });

    return next();
  });
}

module.exports = router;
